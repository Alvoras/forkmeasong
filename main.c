#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>

#define USAGE_STR "Usage : %s [-n] [-s <source file>] [-d <destination file>] [-D <delay>]\n"
#define PROCFILE "procfile"

#define LINE_MAX_SIZE 150

FILE *sfopen(char *path, char *mode){
  FILE *fp = NULL;

  if ( (fp = fopen(path, mode)) == NULL ) {
    printf("Can't open the file\n");
    exit(EXIT_FAILURE);
  }

  return fp;
}
unsigned int getLineQty(FILE *fp){
  int cnt = 0;

  char buf[LINE_MAX_SIZE];

  while (fgets(buf, LINE_MAX_SIZE, fp) != NULL) {
    cnt++;
  }

  rewind(fp);

  return cnt;
}
unsigned int filelen(FILE* fp){
  int len = 0;

  fseek(fp, 0, SEEK_END);
  len = ftell(fp);

  rewind(fp);

  return len;
}
void dbg() {
  printf(">>dbg<<\n");
}
int readnline(FILE* fp, char *buf, int n) {
  int i = 0;
  char fbuf[LINE_MAX_SIZE];

  for (i = 0; i < n; i++) {
    fgets(fbuf, LINE_MAX_SIZE, fp);
    strcat(buf, fbuf);
  }

  return strlen(buf);
}
int main(int argc, char *argv[]) {
  // COUNTER
  int i = 0;
  int j = 0;
  int opt = 0;

  // DATA
  int linePerBloc = 0;
  int lineTotal = 0;
  int childQty = 0;
  int *childArray = NULL;
  int **fdArray = NULL;
  char *buf = NULL;
  unsigned int readlen = 0;
  int dstLen = 0;
  int delay = 1; // default 1 second

  // FILE
  FILE *fp = NULL;
  char *srcFilePath = NULL;
  char *dstFilePath = NULL;
  FILE *procFp = NULL;

  if( argc < 4 ) printf(USAGE_STR, argv[0]), exit(EXIT_FAILURE);

  while ( (opt = getopt(argc, argv, "n:s:d:D:")) != -1 ) {
        switch (opt) {
            case 'n':
              linePerBloc = atoi(optarg);

              if( !linePerBloc ) printf(USAGE_STR, argv[0]), exit(EXIT_FAILURE);
              break;
           case 's':
              srcFilePath = optarg;
              break;
           case 'd':
              dstFilePath = optarg;

              if (access(dstFilePath, F_OK) != -1) {
                unlink(dstFilePath);
              }
              break;
            case 'D':
               delay = abs(atoi(optarg));
               break;
           default:
              printf(USAGE_STR, argv[0]);
              exit(EXIT_FAILURE);
              break;
        }
  }

  fp = sfopen(srcFilePath, "rb");

  lineTotal = getLineQty(fp);

  childQty = lineTotal/linePerBloc;

  if (lineTotal%linePerBloc != 0) {
    childQty++;
  }

  printf("%d\n", childQty);

  rewind(fp);

  childArray = malloc(sizeof(pid_t)*childQty);

  fdArray = malloc(sizeof(int*)*childQty);

  buf = malloc(sizeof(char)*LINE_MAX_SIZE*linePerBloc);

  for (i = 0; i < childQty; i++) {
    *buf = 0;
    fdArray[i] = malloc(sizeof(int)*2);

    readlen = readnline(fp, buf, linePerBloc);
    // '\0'
    readlen++;

    dstLen = strlen(dstFilePath);
    // '\0'
    dstLen++;

    if (pipe(fdArray[i]) == -1) {
      printf("Pipe creation failed\n");
      exit(EXIT_FAILURE);
    }

    childArray[i] = fork();

    if (childArray[i] != 0) {
      //printf("JE SUIS LE PÈRE %u\n", getpid());
      // Father function
      printf("Fils %u créé\n", childArray[i]);

      close(fdArray[i][0]);

      write(fdArray[i][1], buf, readlen*sizeof(char));
      write(fdArray[i][1], dstFilePath, dstLen*sizeof(char));

      close(fdArray[i][1]);
    }else{
      // Child
      char *childBuf = NULL;
      childBuf = malloc(readlen*sizeof(char));

      char *dstName = NULL;
      dstName = malloc(dstLen*sizeof(char));

      FILE *childProcFp = NULL;
      FILE *childFp = NULL;
      int crtPid = 0;

      close(fdArray[i][1]);

      read(fdArray[i][0], childBuf, readlen*sizeof(char));
      read(fdArray[i][0], dstName, dstLen*sizeof(char));

      close(fdArray[i][0]);

      while (1) {
        if (access(PROCFILE, F_OK) == -1) {
          printf("Fils %u : Je n'ai pas trouvé le fichier ! Je me rendors...\n", getpid());
          sleep(delay);

          continue;
        }

        childProcFp = sfopen(PROCFILE, "rb");
        childFp = sfopen(dstName, "ab+");

        fscanf(childProcFp, "%u", &crtPid);

        if (crtPid == getpid()) {
          fprintf(childFp, "%s", childBuf);
          fclose(childFp);
          printf("J'ai trouvé le PID %u dans le fichier, c'est mon tour !\n", getpid());
          printf("J'écris dans %s : \n", dstFilePath);
          printf("%s\n", childBuf);
          break;
        }else{
          printf("Fils %u : ce n'est pas mon tour\n", getpid());
          sleep(delay);
        }

      }

      free(dstName);
      free(childBuf);
      free(buf);

      for (j = 0; j < childQty; j++) {
        free(fdArray[j]);
      }

      free(fdArray);

      exit(EXIT_SUCCESS);
    }
  }

  printf("%d fils créés. Création du fichier procfile\n", childQty);
  procFp = sfopen(PROCFILE, "wb");

  for (i = 0; i < childQty; i++) {
    printf("Père : écriture du pid %u\n", childArray[i]);

    rewind(procFp);
    fprintf(procFp, "%u", childArray[i]);
    fflush(procFp);

    waitpid(childArray[i], NULL, WUNTRACED);
  }

  fclose(procFp);

  for (i = 0; i < childQty; i++) {
    free(fdArray[i]);
  }

  free(fdArray);

  free(childArray);

  free(buf);

  fclose(fp);

  printf("Suppression du procfile...\n");
  unlink(PROCFILE);

  printf("\n");
  printf("    ___ _           __  __       _   ___        _           \n");
  printf("   / __(_)_ _  __ _|  \\/  |___  /_\\ | __|__ _ _| |__      \n");
  printf("   \\__ \\ | ' \\/ _` | |\\/| / -_)/ _ \\| _/ _ \\ '_| / /  \n");
  printf("   |___/_|_||_\\__, |_|  |_\\___/_/ \\_\\_|\\___/_| |_\\_\\ \n");
  printf("              |___/                               v1.0      \n");
  printf("                                                            \n");
  printf("\n");

  return 0;
}
